## Planck 6502 software

This folder contains the software for the Planck computer.
The `drivers`folder contains assembly drivers for the hardware.

The `fos` folder contains a modified version of [TaliForth 2](https://github.com/scotws/TaliForth2) with a platorm file made for the planck. I ported TaliForth code to [CA65](https://cc65.github.io/doc/ca65.html) because I find it more flexible.

Finally the PAL directory contains Wincupl code to program the programmable logic chips on the backplane and on the processor board.


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.