## Planck 6502 sound card

This is a sound card based on the YM3892 OPL2 sound chip.

![OPL2 board](opl2-board.jpg)

![3D render](https://planck6502.com/fabrication/opl2_board-3D_top.png)

You can [download the schematic](https://planck6502.com/fabrication/opl2_board-schematic.pdf) and the [gerber files](https://planck6502.com/fabrication/opl2_board-zip.zip)

[Documentation](https://planck6502.com/Hardware/opl2/)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.