## Planck 6502 RAM board

This is a banked RAM board for the Planck 6502 computer.


![3D render](https://planck6502.com/fabrication/ram_board-3D_top.png)

You can [download the schematic](https://planck6502.com/fabrication/ram_board-schematic.pdf) and the [gerber files](https://planck6502.com/fabrication/ram_board-zip.zip)

[Documentation](https://planck6502.com/Hardware/ram/)


When active, it gives access to a bank or RAM in the $8000-$BFFF address space.

To activate, store a byte with it's high bit set to the first register of the slot where it is placed.

For example 
lda #$80
sta $FFD0
will activate bank 0

There are 32 banks. You can choose which one is active by setting the first 5 bits of the data when storing to the first register. There is no way to get the currently active bank.

When the RAM card is active, it will respond to requests in the $8000 to $BFFF range instead of the ROM

lda #$8E
sta $FFD0
will activate the 14th bank

Setting the high bit to 0 will deactivate the banked RAM

For example the following code will deactivate the banked RAM and restore the ROM :

lda #0
sta $FFD0

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.