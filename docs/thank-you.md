---
layout: page
---
# Purchase complete

Your payment went through and I (probably) received an email telling me that I sold a thing! So next thing you know, I'll be counting capacitors, putting them in little bags, cutting bands of resistors of different values and writing the value on the little paper tabs, trying not to forget the diodes, programming the ICs, putting them in protecting bags, then get them out to test them because I forgot, put them back in, solder the surface mount components if there are any, put it all in one big box with your name and address on it, then, quickly recapping everything in my head and with a bit of doubt as to whether everything is in there, seal the envelope.

Since it's probably too late for the post office by this time, I'll put it near the door so that I don't forget it in the morning. Then I'll go ask the not so nice lady at the post office to please send this for me, and she'll say "ok but that'll be ${A LOT}". After paying she'll take the package, which will start its way to its (hopefully) happy new owner, YOU!

In other words,

<p style="font-size:3em; text-align:center">Thanks for your purchase!</p>

<p style="font-size:3em; text-align:center">❣◕ ‿ ◕❣</p>