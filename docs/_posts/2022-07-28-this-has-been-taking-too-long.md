---
layout: post
title:  "This has been taking too long 🙁"
excerpt_separator: <!--more-->
---

It's been a while, the waitlist [on Tindie](https://www.tindie.com/products/jfoucher/planck-6502-computer/) keeps growing and there's not much I've been able to do about it. Apart from supply chain issues for electroniccomponents, which may imply a new board revision in the near future, I have also moved to a house that has needed way more work done than expected. It's been over 6 months and not everything is ready yet.
<!--more-->

I do not want to store components and prepare packages in a space that is often full of dust and other worksite inconveniences.

However, I am hopefull the next few weeks will finally let me have a space to store everything I need and to prepare orders. So keep an eye on the Tindie listing if you are interested.

In the meantime, remember that the project is completely open source and open hardware, so head on over to [https://jlcpcb.com](JLCPCB) to order some boards, [https://mouser.com](Mouser) for the components and get soldering ! (with JLCPCB you'll get 5 boards, so that's going to be a lot of soldering !)

Otherwise just hang tight, I'll make sure to post an update here and on Tindie when I'm ready to ship new orders.
